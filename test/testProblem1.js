import {
    inventory
} from '/home/hemanth/MountBlue/CarDealer/input.js';
import {
    problem1
} from '/home/hemanth/MountBlue/CarDealer/problem1.js';
const result = problem1(inventory, 5);
console.log(`Car ${result.id} is a *${result.car_year}* *${result.car_make}* *${result.car_model}*`);
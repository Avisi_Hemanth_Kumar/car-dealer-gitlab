import {
    inventory
} from '/home/hemanth/MountBlue/CarDealer/input.js';
import {
    problem2
} from '/home/hemanth/MountBlue/CarDealer/problem2.js';
const result = problem2(inventory);
console.log(`Last car is a *${result.car_make}* *${result.car_model}*`);
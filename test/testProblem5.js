import {
    inventory
} from '/home/hemanth/MountBlue/CarDealer/input.js';
import {
    problem4
} from '/home/hemanth/MountBlue/CarDealer/problem4.js';
import {
    problem5
} from '/home/hemanth/MountBlue/CarDealer/problem5.js';
const years = problem4(inventory);
const result = problem5(years);
console.log(result);
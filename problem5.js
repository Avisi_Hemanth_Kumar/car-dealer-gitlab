 export function problem5(years) {
     let olderCar = [],
         count = 0;

     for (let year in years) {
         if (year < 2000) {
             olderCar.push(year);
             count += years[year];
         }
     }
     console.log(`No. of cars before 2000 is ${count}`);
     return olderCar;
 }
export function problem3(inventory) {
    // INSERTION SORT USED.
    let j = 0,
        key = 0;
    for (let i = 1; i < inventory.length; i++) {
        key = inventory[i];
        j = i - 1;
        while (j >= 0 && inventory[j].car_model.toUpperCase() > key.car_model.toUpperCase()) {
            inventory[j + 1] = inventory[j];
            j = j - 1;
        }
        inventory[j + 1] = key;
    }
    return inventory;
}
export function problem4(inventory) {
    let inventoryYears = {};
    inventoryYears["'Car_Year'"] = 'id';
    for (let i = 0; i < inventory.length; i++) {
        if (inventoryYears.hasOwnProperty(inventoryYears[inventory[i].car_year]) === false) {
            inventoryYears[inventory[i].car_year] = 1;
        } else {
            inventoryYears[inventory[i].car_year] += 1;
        }
    }
    return inventoryYears;
}